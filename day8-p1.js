const { values } = require('lodash');
const _ = require('lodash');
const { processLineByLine } = require('./helpers');

async function day8_1() {
  const commands = await processLineByLine('day8_input.txt');
  let accumulator = 0;
  let index = 0;
  const executed = [];

  while (index <= commands.length) {
    let command = commands[index].split(' ')[0];
    let value = parseInt(commands[index].split(' ')[1]);

    if (executed.includes(index)) {
      break;
    }

    executed.push(index);
    switch (command) {
      case 'acc':
        accumulator += value;
        index++;
        break;
      case 'jmp':
        index += value;
        break;
      case 'nop':
        index++;
        break;
    }
  }

  console.log(accumulator);
  // answer = 1684
}

module.exports = { day8_1 };