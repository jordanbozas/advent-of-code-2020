const { day1_1_test } = require('./day1-p1');

test('get result for day1', () => {
  expect(day1_1_test()).toBe(751776);
});