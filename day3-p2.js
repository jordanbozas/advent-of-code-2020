const _ = require('lodash');
const { processLineByLine } = require('./helpers');

async function day3_2() {
  const map = await processLineByLine('day3_input.txt');

  const path1 = checkPath(1, 1, map);
  const path2 = checkPath(3, 1, map);
  const path3 = checkPath(5, 1, map);
  const path4 = checkPath(7, 1, map);
  const path5 = checkPath(1, 2, map);
  
  console.log(path1 * path2 * path3 * path4 * path5);
  // answer = 5872458240
}

function checkPath(stepX, stepY, map) {
  const lineLength = map[0].length - 1;
  let x = 0;
  let y = 0;
  let trees = 0;

  while (y < map.length-1) {
    if (x + stepX > lineLength) {
      x = x + stepX - lineLength - 1;
    } else {
      x = x + stepX;
    }
    y = y + stepY;
    if (map[y][x] === '#') {
      trees++;
    }
  }

  return trees;
}

module.exports = { day3_2 };