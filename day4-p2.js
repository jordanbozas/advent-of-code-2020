const _ = require('lodash');
const { process } = require('./helpers');

function day4_2() {
  const rawPassports = process('day4_input.txt');
  const requiredFields = ['byr', 'iyr', 'eyr', 'hgt', 'hcl', 'ecl', 'pid'];
  const optionalFields = ['cid'];
  const passports = rawPassports.split('\r\n\r\n');
  let validPassports = 0;

  passports.map(passport => {
    const fields = passport.replace(new RegExp('\r\n','g'), ' ').split(' ');

    const fieldKeys = fields.map(field => {
      return field.split(':')[0];
    });

    const isValid = requiredFields.every( key => fieldKeys.includes(key) );

    if (isValid) {
      const validFields = validatePassportFields(fields);

      if (validFields) {
        validPassports++;
      }
    }
  });

  console.log(validPassports);
  // answer = 131
}

function validatePassportFields(fields) {
  let validData = true;
  fields.map(field => {
    const key = field.split(':')[0];
    const value = field.split(':')[1];

    switch (key) {
      case 'byr':
        if (value.length !== 4 || value < 1920 || value > 2002) {
          validData = false;
        } 
        break;
      case 'iyr':
        if (value.length !== 4 || value < 2010 || value > 2020) {
          validData = false;
        } 
        break;
      case 'eyr':
        if (value.length !== 4 || value < 2020 || value > 2030) {
          validData = false;
        } 
        break;
      case 'hgt':
        const height = value.substr(0, value.length-2);
        if (value.includes('cm')) {
          if (height < 150 || height > 193) {
            validData = false;
          } 
        } else if (value.includes('in')) {
          if (height < 59 || height > 76) {
            validData = false;
          } 
        } else {
          validData = false;
        }
        break;
      case 'hcl':
        if (value[0] === '#' && value.length === 7) {
          if (!value.substr(1).match(/^[0-9a-f]*$/g)) {
            validData = false;
          }
        } else {
          validData = false;
        }
        break;
      case 'ecl':
        if (!['amb', 'blu', 'brn', 'gry', 'grn', 'hzl', 'oth'].some(color => value.includes(color))) {
          validData = false;
        } 
        break;
      case 'pid':
        if (value.length !== 9) {
          validData = false;
        } 
        break;
      default:
        break;
    }
  });

  return validData;
}

module.exports = { day4_2 };