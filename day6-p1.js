const _ = require('lodash');
const { process } = require('./helpers');

function day6_1() {
  const answers = process('day6_input.txt');
  const groups = answers.split('\n\n');
  const sums = [];

  groups.map(group => {
    const groupAnswers = group.replace(new RegExp('\n','g'), '');
    const uniqueAnswers = _.uniq(Array.from(groupAnswers));
    sums.push(uniqueAnswers.length);
  });

  console.log(_.sum(sums));
  // answer = 6726
}

module.exports = { day6_1 };