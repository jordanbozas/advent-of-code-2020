const _ = require('lodash');
const { processLineByLine } = require('./helpers');

async function day5_2() {
  const passes = await processLineByLine('day5_input.txt');
  const seats = [];

  passes.map(seat => {
    const rowPath = seat.substr(0,7);
    const colPath = seat.substr(7);

    const row = getRow(rowPath);
    const col = getColumn(colPath);
    const id = row * 8 + col;

    seats.push({
      id: id,
      row: row,
      col: col
    });
  });

  const minSeat = _.minBy(seats, 'id');
  const maxSeat = _.maxBy(seats, 'id');
  let missingSeat = -1;

  for (let i = minSeat.id; i <= maxSeat.id; i++) {
    if (!_.find(seats, { id: i })) {
      missingSeat = i;
      break;
    }
  }

  console.log(missingSeat);
  // answer = 705
}

function getRow(seat) {
  let minIndex = 0;
  let maxIndex = 127;
  Array.from(seat).forEach(p => {
    if (p === 'F') {
      maxIndex = maxIndex - Math.ceil((maxIndex - minIndex) / 2);
    } else if (p === 'B') {
      minIndex = minIndex + Math.ceil((maxIndex - minIndex) / 2);
    }
  });
  return minIndex;
}

function getColumn(seat) {
  let minIndex = 0;
  let maxIndex = 7;
  Array.from(seat).forEach(p => {
    if (p === 'L') {
      maxIndex = maxIndex - Math.ceil((maxIndex - minIndex) / 2);
    } else if (p === 'R') {
      minIndex = minIndex + Math.ceil((maxIndex - minIndex) / 2);
    }
  });
  return minIndex;
}

module.exports = { day5_2 };