const _ = require('lodash');
const { process } = require('./helpers');

function day4_1() {
  const rawPassports = process('day4_input.txt');
  const requiredFields = ['byr', 'iyr', 'eyr', 'hgt', 'hcl', 'ecl', 'pid'];
  const optionalFields = ['cid'];
  const passports = rawPassports.split('\r\n\r\n');
  let validPassports = 0;

  passports.map(passport => {
    const fields = passport.replace(new RegExp('\r\n','g'), ' ').split(' ');

    const fieldKeys = fields.map(field => {
      return field.split(':')[0];
    });

    const isValid = requiredFields.every( key => fieldKeys.includes(key) );

    if (isValid) {
      validPassports++;
    }
  });

  console.log(validPassports);
  // answer = 210
}

module.exports = { day4_1 };