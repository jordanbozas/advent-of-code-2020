const _ = require('lodash');
const { processLineByLine } = require('./helpers');

async function day3_1() {
  const map = await processLineByLine('day3_input.txt');

  const stepX = 3;
  const stepY = 1;
  const lineLength = map[0].length - 1;
  let x = 0;
  let y = 0;
  let trees = 0;

  while (y < map.length-1) {
    if (x + stepX > lineLength) {
      x = x + stepX - lineLength - 1;
    } else {
      x = x + stepX;
    }
    y = y + stepY;
    if (map[y][x] === '#') {
      trees++;
    }
  }

  console.log(trees);
  // answer = 176
}

module.exports = { day3_1 };