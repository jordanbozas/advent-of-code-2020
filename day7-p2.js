const _ = require('lodash');
const { processLineByLine } = require('./helpers');

async function day7_2() {
  const rawRules = await processLineByLine('day7_input.txt');
  const BAG_TYPE = 'shiny gold';
  let root = [];
  let bagCount = 0;

  const rules = rawRules.map(rule => {
    const ruleList = splitRule(rule);
    const goldBag = _.findIndex(ruleList, bag => bag.bag.includes(BAG_TYPE));

    if (goldBag === 0) {
      root = ruleList;
    }
    
    return ruleList;
  });

  root.slice(1).forEach(alt => {
    bagCount = bagCount + alt.count + alt.count * getBagCount(rules, alt.bag);
  });

  console.log(bagCount);

  // answer = 50100
}

function getBagCount(rules, bag) {
  let count = 0;
  let branch = _.find(rules, r => r[0].bag === bag);
  if (branch) {
    branch.slice(1).forEach(b=> {
      if (b.count > 0) {
        count = count + b.count + b.count * getBagCount(rules, b.bag);
      }
    });
  }

  return count;
}

function splitRule(rule) {
  const ruleList = [];

  const head = rule.split('contain')[0];
  const tail = rule.split('contain')[1].split(',');

  ruleList.push(trimRule(head));
  tail.forEach(rule => {
    ruleList.push(trimRule(rule));
  })

  return ruleList;
}

function trimRule(rule) {
  const trimmed = rule.replace('bags', '').replace('bag', '').replace('.', '').trim();
  return getCount(trimmed);
}

function getCount(rule) {
  const ruleWithCount = {};
  if (rule === 'no other') {
    ruleWithCount.count = 0;
    ruleWithCount.bag = '';
  } else if (isNaN(rule[0])) {
    ruleWithCount.count = 1;
    ruleWithCount.bag = rule;
  } else {
    ruleWithCount.count = parseInt(rule[0]);
    ruleWithCount.bag = rule.substr(2);
  }
  return ruleWithCount;
}

module.exports = { day7_2 };