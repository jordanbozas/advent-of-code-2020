const _ = require('lodash');
const { process } = require('./helpers');

function day6_2() {
  const answers = process('day6_input.txt');
  const groups = answers.split('\n\n');
  const sums = [];

  groups.map(group => {
    const groupAnswers = group.replace(new RegExp('\n','g'), ',').split(',');
    const people = groupAnswers.length;
    const allAnswers = groupAnswers.join('');
    const everyAnswer = [];
    const uniqueAnswers = _.uniq(Array.from(allAnswers));
    uniqueAnswers.forEach(a=> {
      const howMany = _.filter(Array.from(allAnswers), i => i === a);
      if (howMany.length === people) {
        everyAnswer.push(a);
      }
    })
    sums.push(everyAnswer.length);
  });

  console.log(_.sum(sums));
  // answer = 3316
}

module.exports = { day6_2 };