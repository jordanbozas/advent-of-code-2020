const { values } = require('lodash');
const _ = require('lodash');
const { processLineByLine } = require('./helpers');

async function day8_2() {
  const commands = await processLineByLine('day8_input.txt');
  let accumulator = 0;
  let index = 0;
  const executed = [];
  let brokenStep = -1;

  while (index <= commands.length) {
    let command = commands[index].split(' ')[0];
    let value = parseInt(commands[index].split(' ')[1]);

    console.log(commands[index]);
    console.log(index);
    console.log(executed);

    if (executed.includes(index)) {
      if (brokenStep !== -1) {
        brokenStep = index;
        console.log(brokenStep);
        let lastCommand = commands[brokenStep].split(' ')[0];
        let lastValue = parseInt(commands[brokenStep].split(' ')[1]);
        if (lastCommand === 'jmp') {
          lastCommand = 'nop';
        } else if (lastCommand === 'nop') {
          lastCommand = 'jmp';
        }
        if (lastCommand === 'jmp' || lastCommand === 'nop') {
          executed.pop();
          command = lastCommand;
          value = lastValue;
          index = brokenStep;
        }
        console.log(commands[index]);
        console.log(index);
        console.log(executed);
      } else {
        brokenStep = executed[executed.length - 1];
        console.log(brokenStep);
        let lastCommand = commands[brokenStep].split(' ')[0];
        let lastValue = parseInt(commands[brokenStep].split(' ')[1]);
        if (lastCommand === 'jmp') {
          lastCommand = 'nop';
        } else if (lastCommand === 'nop') {
          lastCommand = 'jmp';
        }
        executed.pop();
        command = lastCommand;
        value = lastValue;
        index = brokenStep;
        console.log(commands[index]);
        console.log(index);
        console.log(executed);
      }
    }

    executed.push(index);
    switch (command) {
      case 'acc':
        accumulator += value;
        index++;
        break;
      case 'jmp':
        index += value;
        break;
      case 'nop':
        index++;
        break;
    }
  }

  console.log(accumulator);
  // answer = ??
}

module.exports = { day8_2 };