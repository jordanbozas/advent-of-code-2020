const _ = require('lodash');
const { processLineByLine } = require('./helpers');

async function day7_1() {
  const rawRules = await processLineByLine('day7_input.txt');
  const BAG_TYPE = 'shiny gold';
  const alternatives = [];
  const goldAlternatives = [];

  const rules = rawRules.map(rule => {
    const ruleList = splitRule(rule);
    const goldBag = _.findIndex(ruleList, bag => bag.bag.includes(BAG_TYPE));

    if (goldBag > 0) {
      alternatives.push(ruleList[0]);
      goldAlternatives.push(ruleList[0]);
    }
    
    return ruleList;
  });
 
  goldAlternatives.forEach(alt => {
    getBags(rules, alternatives, alt.bag);
  });

  const bags = _.uniq(alternatives.map(b=> b.bag));
  console.log(bags.length);

  // answer = 337
}

function getBags(rules, alternatives, bag) {
  rules.forEach(rule => {
    const altBag = _.findIndex(rule, alt => alt.bag.includes(bag));

    if (altBag > 0) {
      alternatives.push(rule[0]);
      getBags(rules, alternatives, rule[0].bag);
    }
  })
}

function splitRule(rule) {
  const ruleList = [];

  const head = rule.split('contain')[0];
  const tail = rule.split('contain')[1].split(',');

  ruleList.push(trimRule(head));
  tail.forEach(rule => {
    ruleList.push(trimRule(rule));
  })

  return ruleList;
}

function trimRule(rule) {
  const trimmed = rule.replace('bags', '').replace('bag', '').replace('.', '').trim();
  return getCount(trimmed);
}

function getCount(rule) {
  const ruleWithCount = {};
  if (rule === 'no other') {
    ruleWithCount.count = 0;
    ruleWithCount.bag = '';
  } else if (isNaN(rule[0])) {
    ruleWithCount.count = 1;
    ruleWithCount.bag = rule;
  } else {
    ruleWithCount.count = parseInt(rule[0]);
    ruleWithCount.bag = rule.substr(2);
  }
  return ruleWithCount;
}

module.exports = { day7_1 };