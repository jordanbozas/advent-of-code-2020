# AoC 2020

My approach to Advent of Code 2020 using JS to solve the problems.

[Leaderboard](https://adventofcode.com/2020/leaderboard/private/view/690297)

## Usage

```bash
npx run-func day4-p1.js day4_1
```

## Contributing
Pull requests are welcome. Hack away.

## License
[MIT](https://choosealicense.com/licenses/mit/)